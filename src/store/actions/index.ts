import axios from 'axios';
import * as loaderTypes from '../actionsTypes';

export default function fetchTest(values: {amount: any; category: any; difficulty: any}) {
    return (dispatch: any) => {
        dispatch({
            type: loaderTypes.IS_LOADING,
            isLoading: true,
        });
        const url = `https://opentdb.com/api.php?
                        amount=${values.amount}&
                        category=${values.category}&
                        difficulty=${values.difficulty}`;
        axios
            .get(url)
            .then((response) => {
                if (typeof response === 'object') {
                    if (response.data.results.length === 0) {
                        dispatch({
                            type: loaderTypes.IS_LOADING_ERROR,
                            isLoadingError: true,
                        });
                    }
                    dispatch({
                        type: loaderTypes.FETCH_TEST,
                        test: response.data.results,
                    });
                    dispatch({
                        type: loaderTypes.IS_LOADING,
                        isLoading: false,
                    });
                }
            })
            .catch((error) => {
                console.error(error);
                dispatch({
                    type: loaderTypes.IS_LOADING_ERROR,
                    isLoadingError: true,
                });
                dispatch({
                    type: loaderTypes.FETCH_TEST,
                    test: [],
                });
                dispatch({
                    type: loaderTypes.IS_LOADING,
                    isLoading: false,
                });
            });
    };
}
