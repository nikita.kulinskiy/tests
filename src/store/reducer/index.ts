import * as loaderTypes from '../actionsTypes';

const initialState = {
    test: [],
    isLoading: false,
    isLoadingError: false,
};

export const reducer = (state = initialState, action: any) => {
    switch (action.type) {
        case loaderTypes.FETCH_TEST:
            return {
                ...state,
                test: action.test,
            };
        case loaderTypes.IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading,
            };
        case loaderTypes.IS_LOADING_ERROR:
            return {
                ...state,
                isLoadingError: action.isLoadingError,
            };
        default:
            return state;
    }
};

export default reducer;
