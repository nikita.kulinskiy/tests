import React from 'react';
import ReactDOM from 'react-dom';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './store/reducer';

import Main from './pages/Main';
import NavBar from './components/NavBar';
import Error from './pages/Error';
import Test from './pages/Main/components/Test';

const store = createStore(reducer, compose(applyMiddleware(thunk)));

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Provider store={store}>
                <NavBar />
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route exact path="/test" component={Test} />
                    <Route exact component={Error} />
                </Switch>
            </Provider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root'),
);
