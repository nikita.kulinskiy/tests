import React from 'react';

const Error = () => (
    <div className="container">
        <h1>404</h1>
    </div>
);

export default Error;
