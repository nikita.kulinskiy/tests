import {useSelector} from 'react-redux';

export default function useStateLoadingError() {
    return useSelector((state: {isLoadingError?: boolean}) => state.isLoadingError);
}
