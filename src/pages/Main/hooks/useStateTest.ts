import {useSelector} from 'react-redux';

export default function useStateTest() {
    return useSelector((state: {test?: any}) => state.test);
}
