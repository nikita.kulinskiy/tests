import {useSelector} from 'react-redux';

export default function useStateLoading() {
    return useSelector((state: {isLoading?: boolean}) => state.isLoading);
}
