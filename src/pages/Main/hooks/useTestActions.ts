import {useDispatch} from 'react-redux';
import fetchTest from '../../../store/actions';

export default function useTestAction() {
    const dispatch = useDispatch();
    return {
        fetchTest: (values: any) => dispatch(fetchTest(values)),
    };
}
