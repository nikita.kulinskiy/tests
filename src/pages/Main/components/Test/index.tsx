import React, {useState} from 'react';
import {v4 as uuidv4} from 'uuid';

import useStateTest from '../../hooks/useStateTest';

const Test = () => {
    const test = useStateTest();
    const [testDone, setTestDone] = useState(false);
    const [trues, setTrues] = useState(0);
    let counter = 0;
    let trueCounter = 0;
    if (testDone) {
        return (
            <div className="container">
                <h1>{`${trues} / ${test.length}`}</h1>
            </div>
        );
    }

    const shuffle = (array: any[]) => {
        const randomSortIndex = 0.5;
        array.sort(() => Math.random() - randomSortIndex);
    };

    const cards = test.map((value: any) => {
        const allAnswersShuffle = Array.from([...value.incorrect_answers, value.correct_answer]);
        shuffle(allAnswersShuffle);
        const uuidName = uuidv4();
        const checker = (e: any) => {
            // eslint-disable-next-line no-unused-vars
            counter += 1;
            const radioButtons = document.getElementsByName(uuidName);
            for (let q = 0; q < radioButtons.length; q++) {
                // @ts-ignore
                radioButtons[q].disabled = true;
            }
            if (e.target.labels[0].innerText === value.correct_answer) {
                e.target.labels[0].style.color = 'green';
                // eslint-disable-next-line no-unused-vars
                trueCounter += 1;
            } else {
                e.target.labels[0].style.color = 'red';
            }
            if (counter === test.length) {
                setTrues(trueCounter);
                setTestDone(true);
            }
        };

        // eslint-disable-next-line no-shadow
        const answers = allAnswersShuffle.map((value) => {
            const uuid = uuidv4();
            return (
                <div className="form-check" key={uuid}>
                    <input
                        className="form-check-input"
                        type="radio"
                        name={uuidName}
                        id={uuid}
                        onChange={(e) => checker(e)}
                    />
                    <label className="form-check-label" htmlFor={uuid}>
                        {React.createElement('div', {dangerouslySetInnerHTML: {__html: value}})}
                    </label>
                </div>
            );
        });

        return (
            <div className="card mb-2 center" key={uuidv4()}>
                <div className="card-body">
                    <div>Category: {value.category}</div>
                    <p>Difficulty: {value.difficulty}</p>
                    {React.createElement('p', {dangerouslySetInnerHTML: {__html: value.question}})}
                    {answers}
                </div>
            </div>
        );
    });
    return <div className="container">{cards}</div>;
};

export default Test;
