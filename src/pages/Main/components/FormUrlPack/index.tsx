import React from 'react';
import cn from 'classnames';
import {Field, Form, Formik} from 'formik';

import useTestAction from '../../hooks/useTestActions';

import styles from '../../main.module.css';

const FormUrlPack = () => {
    const {fetchTest} = useTestAction();

    return (
        <div className={cn('container', styles.container_center)}>
            <h1>Create Test</h1>
            <Formik
                initialValues={{
                    amount: '10',
                    category: '',
                    difficulty: '',
                }}
                /* eslint-disable-next-line react/jsx-props-no-multi-spaces */
                onSubmit={(values) => {
                    fetchTest(values);
                }}
            >
                {/* eslint-disable-next-line no-unused-vars */}
                {({values}) => (
                    <Form>
                        <div className="mb-3">
                            <label htmlFor="numberOfQuestions" className="form-label">
                                Number of Questions:
                            </label>
                            <Field
                                type="number"
                                name="amount"
                                id="numberOfQuestions"
                                className="form-control"
                                min="1"
                                max="50"
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="selectCategory" className="form-label">
                                Select Category:
                            </label>
                            <Field as="select" name="category" id="selectCategory" className="form-control">
                                <option value="">Any Category</option>
                                <option value="9">General Knowledge</option>
                                <option value="10">Entertainment: Books</option>
                                <option value="11">Entertainment: Film</option>
                                <option value="12">Entertainment: Music</option>
                                <option value="13">Entertainment: Musicals &amp; Theatres</option>
                                <option value="14">Entertainment: Television</option>
                                <option value="15">Entertainment: Video Games</option>
                                <option value="16">Entertainment: Board Games</option>
                                <option value="17">Science &amp; Nature</option>
                                <option value="18">Science: Computers</option>
                                <option value="19">Science: Mathematics</option>
                                <option value="20">Mythology</option>
                                <option value="21">Sports</option>
                                <option value="22">Geography</option>
                                <option value="23">History</option>
                                <option value="24">Politics</option>
                                <option value="25">Art</option>
                                <option value="26">Celebrities</option>
                                <option value="27">Animals</option>
                                <option value="28">Vehicles</option>
                                <option value="29">Entertainment: Comics</option>
                                <option value="30">Science: Gadgets</option>
                                <option value="31">Entertainment: Japanese Anime &amp; Manga</option>
                                <option value="32">Entertainment: Cartoon &amp; Animations</option>
                            </Field>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="selectDifficulty" className="form-label">
                                Select Difficulty:
                            </label>
                            <Field as="select" name="difficulty" id="selectDifficulty" className="form-control">
                                <option value="">Any Difficulty</option>
                                <option value="easy">Easy</option>
                                <option value="medium">Medium</option>
                                <option value="hard">Hard</option>
                            </Field>
                        </div>
                        <button type="submit" className="btn btn-primary">
                            Generate
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default FormUrlPack;
