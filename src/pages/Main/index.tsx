import React from 'react';

import Test from './components/Test';

import useStateTest from './hooks/useStateTest';
import useStateLoading from './hooks/useStateLoading';
import useStateLoadingError from './hooks/useStateLoadingError';
import FormUrlPack from './components/FormUrlPack';

const Main = () => {
    const test = useStateTest();
    const isLoading = useStateLoading();
    const isLoadingError = useStateLoadingError();
    if (isLoading) {
        return (
            <div className="d-flex justify-content-center">
                <div className="spinner-border" role="status" />
            </div>
        );
    }
    if (test.length) {
        return <Test />;
    }
    if (isLoadingError) {
        return (
            <div className="d-flex justify-content-center">
                <span>Test not found</span>
            </div>
        );
    }
    return <FormUrlPack />;
};

export default Main;
