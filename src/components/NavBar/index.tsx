import React from 'react';

const NavBar = () => (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
            <a className="navbar-brand" href="/">
                Tests
            </a>
        </div>
    </nav>
);

export default NavBar;
